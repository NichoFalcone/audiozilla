﻿using UnityEngine;

public enum GameState
{
    MainMenu = 0,
    Idle,
    Running,
    GameOver,
    GameWin
}

public class GameManager : Singleton<GameManager>
{
    private GameState m_gameState = GameState.MainMenu;

    public delegate void ChangeStateHandler(GameState _gameState);
    public event ChangeStateHandler OnGameStateChange;


    private void Update()
    {
        #region DebugStuff
        if (Input.GetKeyDown(KeyCode.F1) && m_gameState != GameState.GameWin)
        {
            ChangeGameState(GameState.GameWin);
        }
        if (Input.GetKeyDown(KeyCode.F2) && m_gameState != GameState.GameOver)
        {
            ChangeGameState(GameState.GameOver);
        }
        #endregion
    }

    public void ChangeGameState(GameState _gameState)
    {
        m_gameState = _gameState;

        switch (_gameState)
        {
            case GameState.MainMenu:
                break;
            case GameState.Idle:
                break;
            case GameState.Running:
                break;
            case GameState.GameOver:
                break;
            default:
                break;
        }   

        if (OnGameStateChange != null)
            OnGameStateChange(m_gameState);
    }
}
