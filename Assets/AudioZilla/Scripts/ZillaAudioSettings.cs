﻿using UnityEngine;
using UnityEngine.Audio;

namespace AudioZilla
{
    [System.Serializable]
    public class ZillaAudioSettings
    {
        [SerializeField, MinMax(0, 1, ShowEditRange = true)]
        private Vector2 m_volume = new Vector2(1,1);
        [SerializeField, MinMax(0, 3, ShowEditRange = true)]
        private Vector2 m_pitch = new Vector2(1,3);
        [SerializeField]
        private AudioFade m_fadeIn = new AudioFade();
        [SerializeField]
        private AudioFade m_fadeOut = new AudioFade();

        public float Volume {
            get => Random.Range(m_volume.x, m_volume.y);
        }

        public float Pitch {
            get => Random.Range(m_pitch.x, m_pitch.y);
        }

        public AudioFade FadeIn {
            get => m_fadeIn;
        }

        public AudioFade FadeOut {
            get => m_fadeOut;
        }


        public ZillaAudioSettings()
        {
            this.m_volume   = new Vector2(1, 1);
            this.m_pitch    = new Vector2(1, 1);
            this.m_fadeIn   = new AudioFade();
            this.m_fadeOut  = new AudioFade();
        }

    }

    [System.Serializable]
    public class AudioFade
    {
        [SerializeField]
        private AnimationCurve m_fadeCurve = new AnimationCurve(new Keyframe(0,0),new Keyframe(1,1));
        [SerializeField,Range(0.1f,0.98f)]
        private float m_timeLinePercentage = 0;
        [SerializeField]
        private float m_interpolationTime = 0;

        public AnimationCurve FadeCurve {
            get => m_fadeCurve;
        }

        public float TimeLinePercentage {
            get => m_timeLinePercentage;
        }

        public float InterpolationTime {
            get => m_interpolationTime;
        }

        public AudioFade()
        {
            this.m_fadeCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
            this.m_timeLinePercentage = 0.1f;
            this.m_interpolationTime = 0;
        }

    }

    [System.Serializable]
    public class ZillaEventSettings
    {
        [SerializeField]
        private AudioRolloffMode m_rolloffMode = AudioRolloffMode.Custom;
        [SerializeField]
        private int m_spatialBlend = 1;
        [SerializeField]
        private int m_maxDistance = 130;
        [SerializeField]
        private int m_reverbZoneMix = 1;
        [SerializeField]
        private AudioMixerGroup m_outputMix = null;

        public int MaxDistance {
            get => m_maxDistance;
        }
        public AudioRolloffMode RolloffMode {
            get => m_rolloffMode;
        }
        public int SpatialBland {
            get => m_spatialBlend;
        }
        public AudioMixerGroup AudioMixGroup {
            get => m_outputMix;
        }
        public int ReverbZoneMix {
            get => m_reverbZoneMix;
        }
    }

}