﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AudioZilla
{
    public enum SourceState
    {
        Waiting,
        Playing,
        Stopped,
    }
         
    [RequireComponent(typeof(AudioSource))]
    public class ZillaSource : MonoBehaviour
    {
        #region Variable-Field
        [SerializeField]
        private IEZilla m_event = null;
        private AudioSource m_audioSource = null;
        private IEnumerator m_playingCoroutine = null;
        private List<ClipInfo> m_clipsToPlay = new List<ClipInfo>();
        [SerializeField]
        private SourceState m_sourceState = SourceState.Waiting;
        private float m_currentTime = 0;
        private ClipInfo m_currentClip;
        private bool relaseAtEnd = false;
        [SerializeField]
        private int m_clipIndex = -1;

        public System.Action OnEventEndHandler;

        public bool NotUsed {
            get => m_event == null && m_sourceState == SourceState.Waiting;
        }

        public IEZilla Event {
            get => m_event;
        }

        public AudioSource AudioSource {
            get => m_audioSource;
        }
        #endregion
        
        private void Awake()
        {
            m_audioSource = GetComponent<AudioSource>();
            m_audioSource.playOnAwake = false;
        }

        private bool TakeNextClip()
        {
            ///Take a clip with the correct method and assign it to the audio source
            if (m_event.ClipOrder == ZillaClipOrder.Random)
            {
                m_clipIndex = Random.Range(0, m_clipsToPlay.Count);
                m_currentClip = m_clipsToPlay[m_clipIndex];
                ///O(n) bad stuff.... is still better use a queue O(1)?
                m_audioSource.clip = m_currentClip.Clip;

                if(m_event.EventType == ZillaEventType.OneShoot)
                {
                    m_clipsToPlay = new List<ClipInfo>() { m_currentClip };
                }
            }

            m_clipIndex = 0;

            if (m_clipIndex > m_clipsToPlay.Count - 1 || m_clipsToPlay[m_clipIndex] == null)
            {
                Release();
                return false;
            }

            m_currentClip = m_clipsToPlay[m_clipIndex];
            m_audioSource.clip = m_currentClip.Clip;

            ///Remove it if we diden't need to make a loop
            if (!m_event.Loop)
            {
                if (m_clipsToPlay[m_clipIndex] != null)
                    m_clipsToPlay.RemoveAt(m_clipIndex);
            }

            ///setup settings
            m_audioSource.volume = m_currentClip.AudioSettings.Volume;
            m_audioSource.pitch = m_currentClip.AudioSettings.Pitch;
            return true;
        }

        IEnumerator Playing()
        {
            if (!m_audioSource.isPlaying)
                m_audioSource.Play();
            while (m_clipsToPlay.Count > 0 || AudioSource.isPlaying)
            {
                //Debug.Log("clip simple : " + m_audioSource.clip.samples + "\nSource current Simple : " + m_audioSource.timeSamples);
                float m_currentPercentage = ((float)m_audioSource.timeSamples / (float)m_audioSource.clip.samples);
                                
                if (m_currentPercentage > m_event.AudioClip[m_clipIndex].AudioSettings.FadeOut.TimeLinePercentage)
                {
                    m_audioSource.loop = m_event.Loop;
                    yield return StartCoroutine(Stopping());

                    if (m_clipsToPlay.Count > 0)
                    {
                        ///Play the current clip
                        ///Take next clip
                        if (TakeNextClip())
                        {
                            Stop();
                            ///and restart state machine
                            Play();
                        }
                    }
                }
                yield return null;
            }

            Stop();
            Release();
        }

        public void AssignEvent(EventZilla _event)
        {
            m_event = ScriptableObject.Instantiate(_event);

            relaseAtEnd = m_event.EventType == ZillaEventType.OneShoot;
            m_clipsToPlay = new List<ClipInfo>(m_event.AudioClip);
            if (!TakeNextClip())
                return;

            m_audioSource.outputAudioMixerGroup = m_event.ZillaEventSettings.AudioMixGroup;
            m_audioSource.maxDistance = m_event.ZillaEventSettings.MaxDistance;
            m_audioSource.rolloffMode = m_event.ZillaEventSettings.RolloffMode;
            OnEventEndHandler = null;
            /// ---- #DEV to review this parameters ----
            //m_audioSource.spatialBlend  = _event.AudioSettings.SpatialBland;
            //m_audioSource.spread        = _event.AudioSettings.Spread;
            //m_audioSource.dopplerLevel  = _event.AudioSettings.DopplerLevel;
            //m_audioSource.dopplerLevel  = _event.AudioSettings.DopplerLevel;
            //m_audioSource.reverbZoneMix = _event.AudioSettings.ReverbZoneMix;
        }

        /// <summary>
        /// Play this event from begin
        /// </summary>
        public void Play()
        {
            if (m_audioSource.clip == null)
                return;
            m_audioSource.time = 0;
            StartPlayingCoroutine();
        }

        /// <summary>
        /// Stop the current event
        /// </summary>
        public void Stop()
        {
            ///take current clip time
            m_currentTime = m_audioSource.time;
            m_audioSource.Stop();
            StopPlayingCoroutine();
        }

        /// <summary>
        /// resume the current event
        /// </summary>
        public void Resume()
        {
            m_audioSource.time = m_currentTime;
            StartPlayingCoroutine();
        }

        private void StartPlayingCoroutine()
        {
            if (m_sourceState != SourceState.Playing && m_playingCoroutine == null)
            {
                m_sourceState = SourceState.Playing;
                m_playingCoroutine = Playing();
                StartCoroutine(m_playingCoroutine);
                StartCoroutine(Fade(m_currentClip.AudioSettings.FadeIn));
            }
        }

        private void StopPlayingCoroutine()
        {
            if (m_sourceState != SourceState.Stopped)
            {
                m_sourceState = SourceState.Stopped;
                if(m_playingCoroutine != null)
                    StopCoroutine(m_playingCoroutine);
                m_playingCoroutine = null;
            }
        }

        public void Release()
        {
            Stop();
            m_sourceState = SourceState.Waiting;
            m_clipsToPlay = null;
            m_event = null;
            m_playingCoroutine = null;
            if (this.OnEventEndHandler != null)
                this.OnEventEndHandler();
        }

        public void SetMute(bool _state)
        {
            m_audioSource.mute = _state;
        }

        public IEnumerator Stopping()
        {
            yield return StartCoroutine(Fade(m_currentClip.AudioSettings.FadeOut));
        }

        public IEnumerator Fade(AudioFade fade)
        {
            float timer = 0;
            while (timer <= fade.InterpolationTime)
            {
                timer = timer + Time.deltaTime;
                float percent = Mathf.Clamp01(timer / fade.InterpolationTime);
                m_audioSource.volume = Mathf.Lerp(fade.FadeCurve.Evaluate(0), fade.FadeCurve.Evaluate((fade.FadeCurve.keys[fade.FadeCurve.length - 1]).time), percent);
                yield return null;
            }
        }
    }
}
