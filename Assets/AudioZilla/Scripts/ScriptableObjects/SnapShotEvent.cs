﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;

[System.Serializable]
public class SnapShotData
{
    [SerializeField]
    private GameState m_gameStateTarget;
    [SerializeField]
    private float m_value = 1.0f;
    public GameState GameStateTarget => m_gameStateTarget;
    public float Value => m_value;
}

[CreateAssetMenu(fileName = "ZillaSnapshot", menuName = "AudioZilla/Snapshot")]
public class SnapShotEvent : ScriptableObject
{
    [SerializeField]
    private AudioMixerSnapshot m_snapShotToActive;
    [SerializeField]
    private List<SnapShotData> m_snapState;

    public void UseSnapshot(GameState _gamestate)
    {
        SnapShotData snDate = m_snapState.Find((x) => x.GameStateTarget == _gamestate);
        if (snDate == null)
            return;
        ///set currentdata
        m_snapShotToActive.TransitionTo(snDate.Value);
    }
}