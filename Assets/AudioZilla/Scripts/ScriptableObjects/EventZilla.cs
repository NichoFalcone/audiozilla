﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine.Serialization;

namespace AudioZilla
{
    [System.Serializable]
    public class ClipInfo
    {
        [HideInInspector]
        public string name;                     ///inspector name
        [SerializeField]
        private AudioClip m_clip;
        [SerializeField]
        private ZillaAudioSettings m_audioSettings;

        public ZillaAudioSettings AudioSettings => m_audioSettings;
        public AudioClip Clip => m_clip;

        public string Name {
            get => name;
            set {
               name = value;
            }
        }

        public ClipInfo()
        {
            this.m_audioSettings = new ZillaAudioSettings();
        }
    }

    [CreateAssetMenu(fileName = "New AZilla Event", menuName = "AudioZilla/EventZilla")]
    public class EventZilla : ScriptableObject, IEZilla
    {
        [SerializeField, ReadOnly]
        private string m_id                         = "";
        [SerializeField]
        private List<ClipInfo> m_audioClips         = new List<ClipInfo>();

        [Space(5),SerializeField]
        private ZillaEventType m_eventType          = ZillaEventType.OneShoot;
        [SerializeField]
        private bool m_loop                         = false;
        [SerializeField]
        private ZillaClipOrder m_clipOrder          = ZillaClipOrder.Sequence;

        [Space(5),SerializeField]
        private ZillaEventSettings m_eventSettings  = null;

        public List<ClipInfo> AudioClip => m_audioClips;
        public ZillaEventType EventType => m_eventType;
        public bool Loop => m_loop;
        public ZillaClipOrder ClipOrder => m_clipOrder;
        public string ID => m_id;
        public ZillaEventSettings ZillaEventSettings => m_eventSettings;

        private void OnValidate()
        {
            GenerateID();

            if (m_audioClips != null && m_audioClips.Count > 0)
            {
                int index = 0;
                foreach (ClipInfo info in m_audioClips)
                {
                    if (info == null)
                        continue;
                    index++;
                    info.Name = info.Clip != null ? info.Clip.name : "Clip " + index;
                }
            }
        }

        private void GenerateID()
        {
            string fondamentalString = name + m_eventType.ToString();
            ///Add name of clip
            if(m_audioClips != null && m_audioClips.Count > 0)
            {
                m_audioClips.ForEach((x) => fondamentalString += x.Clip != null ? x.Clip.name :  " ");
            }
            m_id = HashUtils.GetHashString(fondamentalString);
        }

    }
}