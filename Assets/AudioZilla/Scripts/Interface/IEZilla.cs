﻿using System.Collections.Generic;
using UnityEngine.Audio;

namespace AudioZilla
{
    public enum ZillaEventType
    {
        OneShoot,
        EventInstance,
    }

    public enum ZillaClipOrder
    {
        Sequence,
        Random
    }

    public interface IEZilla
    {
        bool Loop {
            get;
        }

        string ID {
            get;
        }

        List<ClipInfo> AudioClip {
            get;
        }

        ZillaEventType EventType {
            get;
        }
        ZillaClipOrder ClipOrder {
            get;
        }
        ZillaEventSettings ZillaEventSettings{
            get;
        }
    }
}
