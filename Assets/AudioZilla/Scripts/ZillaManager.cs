﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace AudioZilla
{
    public enum Result
    {
        Valid,
        MissingEvent,
        MissingSource,
        OutOfSourceBound
    }

    public enum GroupType
    {
        SFX,
        Music
    }

    public class ZillaManager : Singleton<ZillaManager>
    {
        [SerializeField]
        private int m_voicesSize = 20;
        private List<ZillaSource> m_sourcesList = new List<ZillaSource>();
        [SerializeField]
        private SnapShotEvent[] m_snapShots;
        [SerializeField]
        private AudioMixer m_mixer;

        protected override void Awake()
        {
            base.Awake();
            InitSourcePool();
        }

        private void OnEnable()
        {
            if(GameManager.Instance == null)
            {
                Debug.LogError("Missing game manager");
                return;
            }

            if (m_snapShots != null)
                GameManager.Instance.OnGameStateChange += CheckSnapshotState;
        }


        private void OnDisable()
        {
            if (GameManager.Instance == null)
            {
                Debug.LogError("Missing game manager");
                return;
            }

            if (m_snapShots != null)
                GameManager.Instance.OnGameStateChange -= CheckSnapshotState;
        }

        private void CheckSnapshotState(GameState _gamestate)
        {
            foreach (SnapShotEvent snap in m_snapShots)
            {
                snap.UseSnapshot(_gamestate);
            }
        }

        private void InitSourcePool()
        {
            for (int i = 0; i < m_voicesSize; i++)
            {
                ZillaSource _zillaSource = new GameObject("ZillaSource " + i.ToString()).AddComponent<ZillaSource>();
                _zillaSource.transform.SetParent(transform);
                m_sourcesList.Add(_zillaSource);
            }
        }

        public ZillaSource PlayZEvent(EventZilla _event)
        {
            ZillaSource source = m_sourcesList.Find((x)=> x.NotUsed);
            if (source == null)
            {
                Debug.LogError("Missing source to play this sound");
                return null;
            }
            source.AssignEvent(_event);
            source.Play();
            return source;
        }


        public ZillaSource PlayZEvent(EventZilla _event, Transform _attacher)
        {
            ZillaSource source = m_sourcesList.Find((x) => x.NotUsed);
            if (source == null)
            {
                Debug.LogError("Missing source to play this sound");
                return null;
            }
            source.AssignEvent(_event);
            AttachEvent(source, _attacher);
            source.Play();
            return source;
        }

        public ZillaSource PlayZEvent(EventZilla _event, System.Action onEndFunc)
        {
            ZillaSource source = m_sourcesList.Find((x) => x.NotUsed);
            if (source == null)
            {
                Debug.LogError("Missing source to play this sound");
                return null;
            }
            source.AssignEvent(_event);
            source.Play();
            source.OnEventEndHandler = new System.Action(() => onEndFunc());
            return source;
        }

        public Result StopZEvent(IEZilla _event)
        {
            ZillaSource source = m_sourcesList.Find((x) => x.Event.ID == _event.ID && x.AudioSource.isPlaying);
            if(source != null)
            {
                source.Stop();
                return Result.Valid;
            }
            return Result.MissingEvent;
        }

        public void PlayOneShoot(EventZilla _event)
        {
            ZillaSource source = m_sourcesList.Find((x) => x.NotUsed);
            if(source == null)
            {
                Debug.LogError("Missing source to play this sound");
                return;
            }
            source.AssignEvent(_event);
            source.Play();
        }

        public void PlayOneShoot(EventZilla _event, Vector3 _position)
        {
            ZillaSource source = m_sourcesList.Find((x) => x.NotUsed);
            if (source == null)
            {
                Debug.LogError("Missing source to play this sound");
                return;
            }
            source.AssignEvent(_event);
            source.transform.position = _position;
            source.Play();
        }

        public void SetGroupAtValue(GroupType groupIndex, float _value)
        {
            m_mixer.SetFloat(groupIndex.ToString(), Mathf.Log10(_value) *20);
        }

        public void PlayOneShoot(EventZilla _event, Transform attacher)
        {
            ZillaSource source = m_sourcesList.Find((x) => x.NotUsed);
            if (source == null)
            {
                Debug.LogError("Missing source to play this sound");
                return;
            }
            source.AssignEvent(_event);
            AttachEvent(source, attacher);
            source.Play();
        }

        public void AttachEvent(ZillaSource _source, Transform _holder)
        {
            _source.transform.SetParent(_holder);
        }
    }
}
