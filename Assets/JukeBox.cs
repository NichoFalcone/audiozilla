﻿using UnityEngine;
using AudioZilla;


public class JukeBox : MonoBehaviour
{
    [SerializeField]
    private EventZilla m_intro;

    [SerializeField]
    private EventZilla m_loopSection;

    [SerializeField]
    private EventZilla m_badResult;

    [SerializeField]
    private EventZilla m_goodResult;


    private ZillaSource m_loopSource;

    private void Start()
    {
        ZillaManager.Instance.PlayZEvent(m_intro, () =>
        {
            GoOnLoopSection();
         }); 
        GameManager.Instance.OnGameStateChange += CheckGameState;
    }

    private void OnDisable()
    {

        GameManager.Instance.OnGameStateChange -= CheckGameState;
    }


    private void CheckGameState(GameState _gameState)
    {
        switch (_gameState)
        {
            case GameState.MainMenu:
                break;
            case GameState.Idle:
                break;
            case GameState.Running:
                break;
            case GameState.GameOver:
                PlayBadResult();
                break;
            case GameState.GameWin:
                PlayGoodResult();
                break;
            default:
                break;
        }
    }

    private void GoOnLoopSection()
    {
        m_loopSource = ZillaManager.Instance.PlayZEvent(m_loopSection);
    }

    private void PlayBadResult()
    {
        m_loopSource.Release();
        ZillaManager.Instance.PlayZEvent(m_badResult, () =>
        {
            GoOnLoopSection();
        });
    }


    private void PlayGoodResult()
    {
        m_loopSource.Release();
        ZillaManager.Instance.PlayZEvent(m_goodResult, () =>
        {
            GoOnLoopSection();
        });
    }
}
